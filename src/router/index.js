import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/login'
import Home from '@/components/home'
import NewHome from '@/components/newHome'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/newHome',
      name: 'newHome',
      component: NewHome
    },
    {
      path: '/',
      name: 'login',
      component: Login
    }
  ]
})
